<?php
session_start();
include "perfect_function.php";
include "header.php";

if (!isset($_SESSION['username'])) {
    header("Location: index.php");
}
$table_name = "tests";
$gettestid = _get_testid_from_testname($_SESSION['testname'], $table_name);

?>
<center>
<form method="post" action="createquestion_proc.php">
<?php

    for($i=1;$i<=$_GET['questions'];$i++) { ?>
    
        <label><b>Enter question <?php echo $i ?>: </b></label>
        <input type="text" name="qname<?= $i ?>" placeholder="Enter question"><br>

        <label> Enter letter A: </label>
        <input type="text" name="a<?= $i ?>" placeholder="Enter letter A"> <br>
        <label> Enter letter B: </label>
        <input type="text" name="b<?= $i ?>" placeholder="Enter letter B"> <br>
        <label> Enter letter C: </label>
        <input type="text" name="c<?= $i ?>" placeholder="Enter letter C"> <br>
        <label> Enter letter D: </label>
        <input type="text" name="d<?= $i ?>" placeholder="Enter letter D"> <br>

        <label>Correct answer</label>:
        <select name="ans<?= $i ?>" placeholder="Choose correct answer " >
            <option value="">Select answer for question <?php echo $i ?></option>
            <option value="a">letter A</option>
            <option value="b">letter B</option>
            <option value="c">letter C</option>
            <option value="d">letter D</option> </select> <br><br>
    <?php } ?>

                   
    <button type="submit">
        Done
    </button>
</form>
<br>
<form method="post" action="home.php">
	<button type="submit">
		Cancel
	</button>
    </form>

<?php 
include "footer.php";
?>