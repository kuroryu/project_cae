<!--

OPEN PHP SCRIPT
    session_start();
        if (isset($_SESSION['incorrect_msg'])) {
            if ($_SESSION['incorrect_msg']==1) {
                echo "
                    <center><h4 style='color:red;'>Username or password is incorrect. Please try again.</h4></center>
                ";
                session_unset();
            }elseif ($_SESSION['incorrect_msg']==2) {
                echo "
                    <center><h4 style='color:red;'>Account not registered. Please register to continue.</h4><center>
                ";
            }

        }
    session_destroy();
CLOSE PHP SCRIPT

<center>
    <form method="post" action="login_proc.php">
        <label>Username:</label> &nbsp;
        <input type="text" name="username" placeholder="Enter username" required><br>
        <label>Password:</label> &nbsp;
        <input type="password" name="password" placeholder="Enter password" required><br>
        <div class="container-login100-form-btn">
    						<button class="login100-form-btn" type="submit">
    							Login
    						</button>
    						<a href = "register.php"> 		
    							<button type ="button" class="login100-form-btn" >
    							Register
    							</button>
    						</a>
    					</div>
    </form>
</center>
-->

<?php
session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>CAE - Computer Aided Examination</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
					<span class="login100-form-title-1">
                        WELCOME TO CAE
                        <br>
                        <h5>Computer Aided Examination<h5>    
					</span>
                </div>
                <?php
                echo "<br>";

                if (isset($_SESSION['incorrect_msg'])) {
                    if ($_SESSION['incorrect_msg']==1) {
                        echo "
                            <center><h4 style='color:red;'>Username or password is incorrect. Please try again.</h4></center>
                        ";
                    }elseif ($_SESSION['incorrect_msg']==2) {
                        echo "
                            <center><h4 style='color:red;'>Account not registered. Please register to continue.</h4><center>
                        ";
                    }
                }
                session_destroy();
                ?>

				<form class="login100-form validate-form" method="post" action="login_proc.php">
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="username" placeholder="Enter username">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="Enter password">
						<span class="focus-input100"></span>
                    </div>
                    

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit">
							Login
						</button>
						&nbsp;&nbsp;
						<a href = "register.php"> 		
							<button type ="button" class="login100-form-btn" >
							Register
							</button>
						</a>
					</div>
				</form>
				<center>Lost account? <b><a href="mailto:caesupport@gmail.com?subject=Lost%20Account">Contact us.</a></b></center>
			</div>
			
		</div>
	</div>
	
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>