<?php
	include "perfect_function.php";

	$table_name = 'torfquestions';

	//get user ID from URL
    $id = $_GET['id'];
    $questionname = $_POST['questionname'];
    $correctanswer = $_POST['correctanswer'];

	$user_editedValues = array(
		"questionname" => $questionname,
		"correctanswer" => $correctanswer,
	);


	update($user_editedValues, $id, $table_name);
	header("Location: torfquestions.php");
?>