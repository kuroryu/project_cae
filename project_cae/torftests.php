<?php
	session_start();
	include "perfect_function.php";
	include "header.php";
	if (!isset($_SESSION['username'])) {
		header("Location: index.php");
	}


	$username = $_SESSION['username'];
	echo "<h1>Hello, ". $username."!</h1>";
?>
<br>

<a href="torfcreatetest.php" class="btn btn-success btn-icon-split">
<span class="icon text-white-50"> 
    <i class="fas fa-plus-circle"></i>
</span>
<span class="text">CREATE TRUE OR FALSE TEST</span>
</a>

<a href="home.php" class="btn btn-success btn-icon-split">
<span class="icon text-white-50"> 
    <i class="far fa-check-circle"></i>
</span>
<span class="text">VIEW MULTIPLE CHOICE TESTS</span>
</a>


<br><br>

<!-- DataTales Example -->
<div class="card shadow mb-4">
<div class="card-header py-3">
    <h3 class="m-0 font-weight-bold text-primary">CREATED  TRUE OR FALSE TESTS</h3>
</div>
<div class="card-body">
    <div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <td>Test Name</td>
                <td>OPTIONS</td>
            </tr>
        </thead>
        <tfoot>
            <tr>
				<td>Test Name</td>
                <td>OPTIONS</td>
            </tr>
        </tfoot>
    <tbody>
    <?php 
    $table_name = "torftests";
        //get all records from users table
        $user_data = get($table_name);
        //fetch result set and pass it to an array (associative)
        foreach ($user_data as $key => $row) {
            $id = $row['test_id'];
			$testname = $row['testname'];
			//$totalquestions = $row['totalquestions'];
			$user_id = $row['user_id'];
    ?>
    <tr> 
        <td><?= $testname ?></td>
        <td> 
        <a href="torfedittest.php?id=<?= $id ?>" class = "btn btn-warning btn-icon-split btn-md"> 
        <span class="icon text-white-50"> 
            <i class="far fa-edit"></i> 
        </span>
        <span class="text">EDIT</span>
        </a>
        &nbsp;
        <a href="torfdeletetest.php?id=<?= $id ?>" class = "btn btn-danger btn-icin-split btn-md">
        <span class="icon text-white-50">
            <i class="fas fa-trash"></i>
        </span>
        <span class="text">DELETE</span>
        </a>
        </td>
    </tr>
    <?php } ?>
    </tbody>
</table>
</div>
</div>
</div>

<?php 
    include "footer.php";
?>

