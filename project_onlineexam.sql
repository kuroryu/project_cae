-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2020 at 01:18 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_onlineexam`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `test_id` int(255) NOT NULL,
  `question_id` int(11) NOT NULL,
  `questionname` varchar(255) NOT NULL,
  `optiona` varchar(255) NOT NULL,
  `optionb` varchar(255) NOT NULL,
  `optionc` varchar(255) NOT NULL,
  `optiond` varchar(255) NOT NULL,
  `correctanswer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`test_id`, `question_id`, `questionname`, `optiona`, `optionb`, `optionc`, `optiond`, `correctanswer`) VALUES
(20, 1, 'q2', '', '', '', '', ''),
(20, 2, 'q2', '', '', '', '', ''),
(27, 3, 'Print for HTML', 'echo', 'print', 'out', 'none', 'd'),
(27, 4, 'What is commend in HTML', '<!-- -->', '/* */', '\\n', 'none', 'a'),
(29, 5, 'what is python', 'web script', 'programming language', 'animal', 'none', 'b'),
(29, 6, 'what is anaconda', 'web script', 'programming language', 'animal', 'none', 'd'),
(30, 7, '1+1 = ?', '1', '2', '3', 'none', 'b'),
(30, 8, '1-1 = ?', '1', '2', '3', 'none', 'd'),
(32, 9, '1+1 = ?', '2', '1', '3', 'none', 'a'),
(32, 10, '2 - 2 = ?', '1', '0', '2', 'none', 'b'),
(32, 11, '5 x 5 = ?', '10', '15', '30', 'none', 'd'),
(33, 12, 'English is a local language', 'yes', 'no', 'either', 'neither', 'b'),
(33, 13, 'English is easy to learn', 'no', 'yes', 'maybe', 'none', 'a'),
(33, 14, 'awdad', 'a', 'b', 'c', 'd', 'd'),
(34, 15, 'Trial', 'A', 'B', 'C', 'D', 'a'),
(34, 16, 'Trial 2', 'A', 'B', 'C', 'D', 'c'),
(35, 17, 'Print for HTML', 'print', 'output', 'echo', 'none', 'd'),
(35, 18, 'HTML is a programming language', 'yes', 'no', 'maybe', 'none', 'b'),
(36, 19, 'output in HTML', 'print', 'echo', 'output', 'none of the above', 'd'),
(36, 20, 'comment in HTML', '//', '/*', '\n', 'none of the above', 'd'),
(37, 21, 'output in PHP', 'print', 'echo', 'output', 'none of the above', 'b'),
(37, 22, 'is PHP a programming language', 'yes', 'no', 'maybe', 'none of the above', 'a'),
(38, 23, 'output in PHP', 'print', 'output', 'echo', 'none', 'c'),
(38, 24, 'comment in PHP', 'n', 'comment', '/*', 'none', 'd'),
(38, 25, 'file extention of php', '.py', '.html', '.php', 'none', 'c'),
(39, 26, '1 + 1 = ?', '1', '2', '3', '0', 'b'),
(39, 27, '3 - 2 = ?', '4', '3', '1', '2', 'c');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `test_id` int(11) NOT NULL,
  `testname` varchar(255) NOT NULL,
  `totalquestions` int(255) NOT NULL,
  `user_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`test_id`, `testname`, `totalquestions`, `user_id`) VALUES
(33, 'English Exam', 3, '3'),
(34, 'Science Exam', 2, '3'),
(38, 'HTML', 3, '13'),
(39, 'MATH', 2, '13');

-- --------------------------------------------------------

--
-- Table structure for table `torfquestions`
--

CREATE TABLE `torfquestions` (
  `test_id` int(255) NOT NULL,
  `question_id` int(11) NOT NULL,
  `questionname` varchar(255) NOT NULL,
  `truee` varchar(255) NOT NULL,
  `falsee` varchar(255) NOT NULL,
  `correctanswer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `torfquestions`
--

INSERT INTO `torfquestions` (`test_id`, `question_id`, `questionname`, `truee`, `falsee`, `correctanswer`) VALUES
(8, 5, 'Boy?', 'True', 'False', 'true'),
(8, 6, 'Small?', 'True', 'False', 'false'),
(8, 7, 'Teen?', 'True', 'False', 'true'),
(8, 8, 'Sleepy?', 'True', 'False', 'true'),
(9, 9, 'Am I a boy?', 'True', 'False', 'true'),
(9, 10, 'Am I small?', 'True', 'False', 'false'),
(10, 11, 'Is my cat a boy?', 'True', 'False', 'true'),
(10, 12, 'I have 3 cats', 'True', 'False', 'false'),
(10, 13, 'I love cats', 'True', 'False', 'true'),
(11, 14, 'Boy?', 'True', 'False', 'true'),
(11, 15, 'Tall?', 'True', 'False', 'true'),
(11, 16, 'Love cats?', 'True', 'False', 'true'),
(12, 17, 'Sweet?', 'True', 'False', 'true'),
(12, 18, 'Salty?', 'True', 'False', 'false'),
(13, 19, 'Yummy?', 'True', 'False', 'true'),
(13, 20, 'Healthy?', 'True', 'False', 'false'),
(14, 21, 'Color is black orange', 'True', 'False', 'false'),
(14, 22, 'Boy?', 'True', 'False', 'true'),
(14, 23, 'Kittens?', 'True', 'False', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `torftests`
--

CREATE TABLE `torftests` (
  `test_id` int(11) NOT NULL,
  `testname` varchar(255) NOT NULL,
  `totalquestions` varchar(255) NOT NULL,
  `user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `torftests`
--

INSERT INTO `torftests` (`test_id`, `testname`, `totalquestions`, `user_id`) VALUES
(10, 'My cat', '3', 3),
(11, 'About the dev', '3', 13),
(14, 'About my cats', '3', 13);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `account_type` int(1) NOT NULL,
  `account_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `email`, `username`, `password`, `account_type`, `account_status`) VALUES
(1, 'Miguel', 'Gamiao', 'dragon@gmail.com', 'dragon', 'dragon', 1, 1),
(2, 'Ten', 'Sai', 'tensai@gmail.com', 'tensai', 'tensai', 0, 1),
(3, 'Miguel', 'Gamiao', 'xdragon@gmail.com', 'xDragon', 'xdragon', 1, 1),
(5, 'miguel', 'gamiao', 'tensai@gmail.com', 'xTensai', 'xtensai', 0, 1),
(7, 'Miguel', 'Gamiao', 'kuroryu@gmail.co', 'kuroryu', 'kuroryu', 0, 1),
(8, 'awd', 'dwad', 'dwa@gmail.com', 'xDragon', 'awd', 0, 1),
(9, 'awd', 'awd', 'xdragon@gmail.com', 'awdwadwadwaw', 'awd', 0, 1),
(10, 'Miguel', 'Gamiao', 'kuroryu@gmail.com', 'kuroryu', 'kuroryu', 1, 1),
(11, 'Miguel', 'Gamiao', 'xxx@gmail.com', 'xxx', 'xxx', 0, 1),
(12, 'MIguel', 'Gamiao', 'kuroryu@gmail.com', 'kuroryu', 'kuroryu', 1, 1),
(13, 'Miguel', 'Gamiao', 'miguel@gmail.com', 'miguel', 'miguel', 1, 1),
(14, 'Miguel', 'Gamiao', 'mgamiao@gmail.com', 'mgamiao', 'mgamiao', 0, 1),
(15, 'Miguel', 'Gamiao', 'miguel@gmail.com', 'miguel', 'miguel', 1, 1),
(16, 'Miguel', 'Gamiao', 'mgamiao@gmail.com', 'mgamiao', 'mgamiao', 0, 1),
(17, 'Miguel', 'Gamiao', 'miguel@gmail.com', 'miguel', 'miguel', 1, 1),
(18, 'Miguel', 'Gamiao', 'mgamiao@gmail.com', 'mgamiao', 'mgamiao', 0, 1),
(19, 'Miguel', 'Gamiao', 'miguel@gmail.com', 'miguel', 'miguel', 1, 1),
(20, 'Miguel', 'Gamiao', 'mgamiao@gmail.com', 'mgamiao', 'mgamiao', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `torfquestions`
--
ALTER TABLE `torfquestions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `torftests`
--
ALTER TABLE `torftests`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `torfquestions`
--
ALTER TABLE `torfquestions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `torftests`
--
ALTER TABLE `torftests`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
